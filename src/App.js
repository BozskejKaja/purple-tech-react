import React, {Component} from 'react';
import logo from './Purple.png';
// import './scss/App.css';
import 'react-widgets/dist/css/react-widgets.css';
import Converter from "./components/Converter/Converter";
import Stats from "./components/Stats/Stats";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            socket: null,
            stats: {max: {}, total: {}},
        }
    }

    componentWillMount() {
        const socket = new WebSocket("ws://localhost:4000");
        socket.onmessage = msg => {
            this.setState({stats: JSON.parse(msg.data)});
        };

        socket.onerror = error => {
            console.error(`ERROR: error connecting to websocket error msg: ${error.message}`);
        };

        socket.onclose = closeEvent => {
            console.info(`INFO: websocket closed, reason: ${closeEvent.reason}, was clean : ${closeEvent.wasClean}, close code: ${closeEvent.code}`);
        };
        this.setState({socket});
    }

    componentWillUnmount() {
        const {socket} = this.state;
        socket && socket.close();
    }

    render() {
        const {stats} = this.state;
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <div className="title">
                        Currency converter
                    </div>
                </header>
                <div className="content-container">
                    <Stats stats={stats}/>
                    <Converter/>
                </div>
            </div>
        );
    }
}

export default App;
