/**
 * Created by Filip Drgoň on 18/03/18.
 */

import React from 'react';
import PropTypes from 'prop-types';

const StatsBlock = ({title, stats}) => {
    return (
        <div className="stats-block">
            <div className="stats-block-title">
                {title}
            </div>
            <div className="stats-block-value">
                {!!stats ? stats : <i className="fa fa-spinner fa-spin"/>}
            </div>
        </div>
    );
};

StatsBlock.propTypes = {
    title: PropTypes.string,
    stats: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default StatsBlock;
