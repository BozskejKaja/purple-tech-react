/**
 * Created by Filip Drgoň on 16/03/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import StatsBlock from "./StatsBlock";

const Stats = ({stats}) => {
    const {max, total} = stats;
    const {totalAmount, totalRequests} = total;
    return (
        <div className="stats">
            <StatsBlock
                title="Most used currency"
                stats={max.name}
            />
            <StatsBlock
                title="Total amount converted"
                stats={totalAmount && totalAmount.toFixed(2)}
            />
            <StatsBlock
                title="Total amount of conversions"
                stats={totalRequests}
            />
        </div>
    );
};

Stats.propTypes = {
    stats: PropTypes.object,
};

export default Stats;
