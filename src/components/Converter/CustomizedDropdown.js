/**
 * Created by Filip Drgoň on 16/03/18.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {currencyList} from "../../utils/currencyList";
import DropdownList from 'react-widgets/lib/DropdownList';

export default class CustomizedDropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.defaultValue,
        }
    }

    render() {
        const {value} = this.state;
        const {selectedHandler, label} = this.props;
        return (
            <div className="dropdown-list">
                <label className="converter-label">
                    {label}
                </label>
                <DropdownList
                    data={currencyList}
                    valueField='id'
                    textField={item => `${item.id} - ${item.name}`}
                    value={value}
                    onChange={value => this.setState({value})}
                    onSelect={selected => selectedHandler(selected)}
                    placeholder={"Select a currency..."}
                    suggest={true}
                    filter="contains"
                />
            </div>
        );
    }
}

CustomizedDropdown.propTypes = {
    selectedHandler: PropTypes.func,
    defaultValue: PropTypes.object,
};