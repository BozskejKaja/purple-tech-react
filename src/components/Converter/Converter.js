/**
 * Created by Filip Drgoň on 16/03/18.
 */

import React, {Component} from 'react';
import CustomizedDropdown from "./CustomizedDropdown";
import {currencyList} from "../../utils/currencyList";


export default class Converter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            from: currencyList.find(currency => currency.id === "USD"),
            to: currencyList.find(currency => currency.id === "EUR"),
            amount: "",
            result: null,
            isFetching: false,
            error: null,
        }
    }

    calculate = async () => {
        const {from, to, amount, isFetching} = this.state;
        if (!isFetching) {
            this.setState({isFetching: true, result: null, error: null});
            try {
                const response = await fetch("http://localhost:4000/conversion", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({from: from.id, to: to.id, amount}),

                });
                const json = await response.json();
                if (response.ok) {
                    this.setState({result: json.result.toFixed(2), isFetching: false, error: null});
                } else this.setState({isFetching: false, error: json.message});
            } catch (ex) {
                console.log(ex);
                this.setState({isFetching: false})
            }
        }
    };


    render() {
        const {amount, result, isFetching, from, to, error} = this.state;
        return (
            <div className="converter-component">
                <div className="converter-top">
                    <div className='converter-input-container'>
                        <label htmlFor="amount" className="converter-label">
                            Amount
                        </label>
                        <input
                            id="amount"
                            className={"converter-input"}
                            value={amount}
                            onChange={e => this.setState({amount: e.target.value})}
                            placeholder={"Amount..."}
                        />
                    </div>
                    <CustomizedDropdown
                        selectedHandler={(from) => this.setState({from})}
                        defaultValue={from}
                        label={"From"}
                    />
                    <i className="converter-arrow fa fa-long-arrow-right"/>
                    <CustomizedDropdown
                        selectedHandler={(to) => this.setState({to})}
                        defaultValue={to}
                        label={"To"}
                    />
                    <button className="converter-button" onClick={this.calculate}>
                        <i className={isFetching ? "fa fa-spinner fa-spin" : "fa fa-arrow-right"}/>
                    </button>
                </div>
                {
                    error ?
                        <div className="converter-bottom error">
                            {error}
                        </div>
                        :
                        <div className="converter-bottom">
                            <div className="currency-value">
                                {result}
                            </div>
                            <div className="currency-name">
                                {result && to.id}
                            </div>
                        </div>
                }
            </div>
        );
    }
}

Converter.propTypes = {};
Converter.defaultProps = {};